﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float pe_force_move = 20f;
    public float pe_force_jump = 400f;
    public float pe_maxVelocity = 4f;

    private bool pe_grounded;
    private Rigidbody2D pe_rgb_body;
    private Animator pe_anm_animat;

    void Awake()
    {
        pe_rgb_body = GetComponent<Rigidbody2D>();
        pe_anm_animat = GetComponent<Animator>();
    }


	// Use this for initialization
	void Start () {
	    
	}
	

    void FixedUpdate()
    {
        Walk_by_key_board();
    }

    void Walk_by_key_board()
    {
        float force_X = 0f;
        float force_Y = 0f;

        float velocity = Mathf.Abs(pe_rgb_body.velocity.x);
        float h = Input.GetAxisRaw("Horizontal");

        if(h > 0)
        {
            if(velocity < pe_maxVelocity)
            {
                if (pe_grounded)
                {
                    force_X = pe_force_move;
                }
                else
                {
                    force_X = pe_force_move * 1.1f;
                }
                
            }
            Vector3 scale = transform.localScale;
            scale.x = 5f;
            transform.localScale = scale;
            pe_anm_animat.SetBool("walk", true);
            
        }
        else if(h < 0)
        {
            if (velocity < pe_maxVelocity)
            {
                if (pe_grounded)
                {
                    force_X = -pe_force_move;
                }
                else
                {
                    force_X = -pe_force_move * 1.1f;
                }
            }
            Vector3 scale = transform.localScale;
            scale.x = -5f;
            transform.localScale = scale;
            pe_anm_animat.SetBool("walk", true);
        }
        else if(h == 0)
        {
           pe_anm_animat.SetBool("walk", false);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (pe_grounded)
            {
                pe_grounded = false;
                force_Y = pe_force_jump;
            }
        }

        pe_rgb_body.AddForce(new Vector2(force_X, force_Y));
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if(target.gameObject.tag == "Ground")
        {
            pe_grounded = true;
        }
    }


}
