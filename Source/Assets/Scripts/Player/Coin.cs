﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Coin : MonoBehaviour {
    private int c_score;
    private Text c_text;

	// Use this for initialization
	void Start () {
        c_text = GetComponent<Text>();
        c_score = 0;
	}
	
	// Update is called once per frame
	void Update () {
	    if(c_score < 0)
        {
            c_score = 0;
        }
        c_text.text = "" + c_score;

	}

    void Add_point(int p)
    {
        c_score += p;
    }
    void Reset()
    {
        c_score = 0;
    }
}
